/*
 * Copyright (C) 2019-2020 Chormi (hofill)
 *
 * This file is part of DeathRecovery.
 *
 * DeathRecovery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeathRecovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with DeathRecovery.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.cubekrowd.deathrecovery;

import lombok.Getter;
import lombok.Setter;
import net.cubekrowd.deathrecovery.commands.Teleport;
import net.cubekrowd.deathrecovery.records.DeathDatabase;
import net.cubekrowd.deathrecovery.records.DeathRecord;
import net.cubekrowd.deathrecovery.records.WaitingDatabase;
import net.cubekrowd.deathrecovery.records.WaitingRecord;
import net.cubekrowd.deathrecovery.storage.DeathFileHandler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.cubekrowd.deathrecovery.commands.Deaths;
import net.cubekrowd.deathrecovery.commands.RestoreInvGive;
import net.cubekrowd.deathrecovery.commands.RestoreInvSee;
import net.cubekrowd.deathrecovery.events.InventoryClick;
import net.cubekrowd.deathrecovery.events.PlayerDeath;
import net.cubekrowd.deathrecovery.events.PlayerJoin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class DeathRecovery extends JavaPlugin {

	@Getter
	@Setter
	private DeathDatabase deathDatabase;

	@Getter
	@Setter
	private WaitingDatabase waitingDatabase;

	public static Logger LOGGER;

	private static DeathRecovery INSTANCE;

	@Getter
	private Timer pruneTimer;

	@Getter
	private Timer saveTimer;

	public static DeathRecovery getInstance() {
		return INSTANCE;
	}

	public void onEnable() {
		INSTANCE = this;
		LOGGER = getLogger();
		registerConfig();
		registerDeathConfig();
		registerEvents();
		registerCommands();
		pruneTimer = Timer.pruneOld(this, this::getDeathDatabase, getConfig().getString("time_between_checks"), getConfig().getString("oldest_death_allowed"));
		pruneTimer.start();
		saveTimer = new Timer(this, getConfig().getString("save_time"), this::saveAllAsync);
		saveTimer.start();
	}

	@Override
	public void onDisable() {
		saveAll();
	}

	public static void tellConsole(String msg) {
		Bukkit.getConsoleSender().sendMessage(msg);
	}

	public void registerCommands() {
		getCommand("deaths").setExecutor(new Deaths());
		getCommand("restoreinvsee").setExecutor(new RestoreInvSee());
		getCommand("restoreinvgive").setExecutor(new RestoreInvGive());
		getCommand("drteleport").setExecutor(new Teleport());
	}

	public void registerEvents() {
		getServer().getPluginManager().registerEvents(new PlayerDeath(), this);
		getServer().getPluginManager().registerEvents(new InventoryClick(), this);
		getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
	}

	public void registerConfig() {
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
	}

	public void registerDeathConfig() {
	    deathDatabase = DeathFileHandler.getInstance().loadStandardDeaths();
		waitingDatabase = DeathFileHandler.getInstance().loadStandardWaiting();
	}

	public void saveAll() {
		deathDatabase.saveToDefaultLocation();
		waitingDatabase.saveToDefaultLocation();
	}

	public void saveAllAsync() {
		// Don't assign null yet, so we can use it in the async section
		List<DeathRecord> deaths;
		if (deathDatabase.isModified()) {
			deaths = new ArrayList<>(deathDatabase.getDeaths());
			deathDatabase.setModified(false);
		} else {
			deaths = null;
		}
		List<WaitingRecord> waiting;
		if (waitingDatabase.isModified()) {
			waiting = new ArrayList<>(waitingDatabase.getRecords());
			waitingDatabase.setModified(false);
		} else {
			waiting = null;
		}
		Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
			if (deaths != null) {
				DeathFileHandler.getInstance().saveDeathDatabase(new DeathDatabase(deaths), DeathDatabase.getDefaultFile());
			}
			if (waiting != null) {
				DeathFileHandler.getInstance().saveWaitingDatabase(new WaitingDatabase(waiting), DeathDatabase.getDefaultFile());
			}
		});
	}
}
