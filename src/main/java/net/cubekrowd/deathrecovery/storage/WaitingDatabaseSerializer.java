package net.cubekrowd.deathrecovery.storage;

import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import net.cubekrowd.deathrecovery.records.WaitingDatabase;
import net.cubekrowd.deathrecovery.records.WaitingRecord;

import java.lang.reflect.Type;
import java.util.List;

public class WaitingDatabaseSerializer implements JsonSerializer<WaitingDatabase>, JsonDeserializer<WaitingDatabase> {

    @Override
    public WaitingDatabase deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if (!jsonElement.isJsonObject()) {
            return null;
        }
        JsonObject obj = jsonElement.getAsJsonObject();
        List<WaitingRecord> waiting = context.deserialize(obj.get("records"), new TypeToken<List<WaitingRecord>>() {}.getType());
        return new WaitingDatabase(waiting);
    }

    @Override
    public JsonElement serialize(WaitingDatabase waitingDatabase, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.add("records", context.serialize(waitingDatabase.getRecords(), new TypeToken<List<WaitingRecord>>() {}.getType()));
        return obj;
    }

}
