package net.cubekrowd.deathrecovery.storage;

import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import net.cubekrowd.deathrecovery.records.DeathDatabase;
import net.cubekrowd.deathrecovery.records.DeathRecord;
import net.cubekrowd.deathrecovery.records.WaitingRecord;

import java.lang.reflect.Type;
import java.util.List;

public class DeathDatabaseSerializer implements JsonSerializer<DeathDatabase>, JsonDeserializer<DeathDatabase> {
    @Override
    public DeathDatabase deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if (!jsonElement.isJsonObject()) {
            return null;
        }
        JsonObject obj = jsonElement.getAsJsonObject();
        List<DeathRecord> records = context.deserialize(obj.get("deaths"), new TypeToken<List<DeathRecord>>() {}.getType());
        return new DeathDatabase(records);
    }

    @Override
    public JsonElement serialize(DeathDatabase deathDatabase, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.add("deaths", context.serialize(deathDatabase.getDeaths(), new TypeToken<List<DeathRecord>>() {}.getType()));
        return obj;
    }
}
