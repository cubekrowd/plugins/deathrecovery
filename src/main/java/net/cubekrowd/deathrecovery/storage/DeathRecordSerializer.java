package net.cubekrowd.deathrecovery.storage;

import com.google.gson.*;
import net.cubekrowd.deathrecovery.records.DeathRecord;
import net.cubekrowd.deathrecovery.records.EncodedItemStack;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DeathRecordSerializer implements JsonSerializer<DeathRecord>, JsonDeserializer<DeathRecord> {

    @Override
    public DeathRecord deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if (!jsonElement.isJsonObject()) {
            return null;
        }
        JsonObject obj = jsonElement.getAsJsonObject();
        UUID ownerUUID = context.deserialize(obj.get("ownerUUID"), UUID.class);
        int deathId = obj.get("deathID").getAsInt();
        int x = obj.get("x").getAsInt();
        int y = obj.get("y").getAsInt();
        int z = obj.get("z").getAsInt();
        UUID worldUUID = context.deserialize(obj.get("deathWorldID"), UUID.class);
        String deathMessage = obj.get("deathMessage").getAsString();
        LocalDateTime time = context.deserialize(obj.get("time"), LocalDateTime.class);
        int itemCount = obj.get("itemCount").getAsInt();
        int indexSize = obj.get("indexSize").getAsInt();
        Map<Integer, EncodedItemStack> items = new HashMap<>();
        for (JsonElement element : obj.get("items").getAsJsonArray()) {
            JsonObject inner = element.getAsJsonObject();
            items.put(inner.get("slot").getAsInt(), context.deserialize(inner.get("item"), EncodedItemStack.class));
        }
        return new DeathRecord(ownerUUID, deathId, x, y, z, worldUUID, deathMessage, time, itemCount, indexSize, items);
    }

    @Override
    public JsonElement serialize(DeathRecord deathRecord, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.add("ownerUUID", context.serialize(deathRecord.ownerUUID, UUID.class));
        obj.addProperty("deathID", deathRecord.deathId);
        obj.addProperty("x", deathRecord.x);
        obj.addProperty("y", deathRecord.y);
        obj.addProperty("z", deathRecord.z);
        obj.add("deathWorldID", context.serialize(deathRecord.deathWorldId, UUID.class));
        obj.addProperty("deathMessage", deathRecord.deathMessage);
        obj.add("time", context.serialize(deathRecord.time, LocalDateTime.class));
        obj.addProperty("itemCount", deathRecord.itemCount);
        obj.addProperty("indexSize", deathRecord.indexSize);
        JsonArray items = new JsonArray();
        for (Map.Entry<Integer, EncodedItemStack> entry : deathRecord.items.entrySet()) {
            JsonObject inner = new JsonObject();
            inner.addProperty("slot", entry.getKey());
            inner.add("item", context.serialize(entry.getValue()));
            items.add(inner);
        }
        obj.add("items", items);
        return obj;
    }

}
