package net.cubekrowd.deathrecovery.storage;

import com.google.gson.*;
import net.cubekrowd.deathrecovery.records.DeathDatabase;
import net.cubekrowd.deathrecovery.DeathRecovery;
import net.cubekrowd.deathrecovery.records.DeathRecord;
import net.cubekrowd.deathrecovery.records.EncodedItemStack;
import net.cubekrowd.deathrecovery.records.WaitingDatabase;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;

public class DeathFileHandler {

    private final static DeathFileHandler INSTANCE = new DeathFileHandler();

    public static DeathFileHandler getInstance() {
        return INSTANCE;
    }

    private final static AtomicBoolean BUSY_SAVING_DEATH = new AtomicBoolean(false);
    private final static AtomicBoolean BUSY_SAVING_WAITING = new AtomicBoolean(false);

    private DeathFileHandler() {}

    private final static Gson GSON = new GsonBuilder()
            .setPrettyPrinting()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssX")
            .registerTypeHierarchyAdapter(EncodedItemStack.class, new ItemStackSerializer())
            .registerTypeAdapter(DeathDatabase.class, new DeathDatabaseSerializer())
            .registerTypeAdapter(WaitingDatabase.class, new WaitingDatabaseSerializer())
            .registerTypeAdapter(LocalDateTime.class, new TimeSerializer())
            .registerTypeAdapter(DeathRecord.class, new DeathRecordSerializer())
            .create();

    public DeathDatabase loadDeathDatabase(File file) {
        try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file))) {
            return GSON.fromJson(reader, DeathDatabase.class);
        } catch (Exception e) {
            DeathRecovery.LOGGER.log(Level.SEVERE, "Couldn't load Death Database!", e);
        }
        return new DeathDatabase(new ArrayList<>());
    }

    public WaitingDatabase loadWaitingDatabase(File file) {
        try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file))) {
            return GSON.fromJson(reader, WaitingDatabase.class);
        } catch (Exception e) {
            DeathRecovery.LOGGER.log(Level.SEVERE, "Couldn't load Death Database!", e);
        }
        return new WaitingDatabase(new ArrayList<>());
    }

    public void saveDeathDatabase(DeathDatabase database, File file) {
        if (BUSY_SAVING_DEATH.compareAndSet(false, true)) {
            try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file))) {
                GSON.toJson(database, writer);
            } catch (Exception e) {
                DeathRecovery.LOGGER.log(Level.SEVERE, "Couldn't save death database!", e);
            } finally {
                BUSY_SAVING_DEATH.set(false);
            }
        }
    }

    public void saveWaitingDatabase(WaitingDatabase database, File file) {
        if (BUSY_SAVING_WAITING.compareAndSet(false, true)) {
            try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file))) {
                GSON.toJson(database, writer);
            } catch (Exception e) {
                DeathRecovery.LOGGER.log(Level.SEVERE, "Couldn't save waiting database!", e);
            } finally {
                BUSY_SAVING_WAITING.set(false);
            }
        }
    }

    /**
     * Loads the DeathDatabase from the configuration folder and automatically handles exceptions.
     * @return New DeathDatabase. It will contain a blank one if any issues occurred.
     */
    public DeathDatabase loadStandardDeaths() {
        DeathRecovery plugin = DeathRecovery.getInstance();
        File file = new File(plugin.getDataFolder(), "deaths.json");

        File old = new File(plugin.getDataFolder(), "deaths.yml");
        if (!file.exists() && old.exists()) {
            DeathRecovery.tellConsole("Old configuration found! Updating...");
            DeathDatabase db = DeathDatabase.convertOld(YamlConfiguration.loadConfiguration(old));
            db.saveToDefaultLocation();
            DeathRecovery.tellConsole("Success!");
            return db;
        }

        if (!file.exists()) {
            try {
                plugin.saveResource("deaths.json", false);
            } catch (Exception ex) {
                DeathRecovery.tellConsole("Cannot create deaths.json!");
            }
        }

        try {
            DeathDatabase database = loadDeathDatabase(file);
            if (database == null) {
                return new DeathDatabase(new ArrayList<>());
            }
            return database;
        } catch (Exception e) {
            DeathRecovery.LOGGER.log(Level.SEVERE, "Error reading deaths.json!", e);
            // Will just be blank
        }
        return new DeathDatabase(new ArrayList<>());
    }

    public WaitingDatabase loadStandardWaiting() {
        File file = new File(DeathRecovery.getInstance().getDataFolder(), "waiting.json");
        if (!file.exists()) {
            try {
                DeathRecovery.getInstance().saveResource("waiting.json", false);
            } catch (Exception ex) {
                DeathRecovery.tellConsole("Cannot create waiting.json!");
            }
        }
        try {
            WaitingDatabase waiting = loadWaitingDatabase(file);
            if (waiting == null) {
                return new WaitingDatabase(new ArrayList<>());
            }
            return waiting;
        } catch (Exception e) {
            DeathRecovery.LOGGER.log(Level.SEVERE, "Error reading deaths.json!", e);
            // Will just be blank
        }
        return new WaitingDatabase(new ArrayList<>());
    }

}
