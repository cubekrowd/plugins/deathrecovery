package net.cubekrowd.deathrecovery.storage;

import com.google.gson.*;
import net.cubekrowd.deathrecovery.records.EncodedItemStack;

import java.lang.reflect.Type;

public class ItemStackSerializer implements JsonSerializer<EncodedItemStack>, JsonDeserializer<EncodedItemStack> {

    @Override
    public EncodedItemStack deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        return EncodedItemStack.fromEncoded(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(EncodedItemStack stack, Type type, JsonSerializationContext context) {
        return new JsonPrimitive(stack.getData());
    }

}
