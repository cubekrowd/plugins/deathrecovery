/*
 * Copyright (C) 2019-2020 Chormi (hofill)
 *
 * This file is part of DeathRecovery.
 *
 * DeathRecovery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeathRecovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with DeathRecovery.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.cubekrowd.deathrecovery.commands;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import net.cubekrowd.deathrecovery.DeathRecovery;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Deaths implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!label.equalsIgnoreCase("deaths")) {
            return true;
        }
        if (args.length != 1) {
            return false;
        }
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only players can use this command!");
            return true;
        }
        Player player = (Player) sender;
        if (!player.hasPermission("deathrecovery.deaths")) {
            player.sendMessage(ChatColor.RED + "The player " + args[0] + " has no history of deaths saved.");
            return true;
        }
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
        var records = DeathRecovery.getInstance().getDeathDatabase().getRecords(offlinePlayer.getUniqueId());
        if (records.isEmpty()) {
            player.sendMessage(ChatColor.RED + "The player " + args[0] + " has no history of deaths saved.");
            return true;
        }

        // Create information for deaths
        player.sendMessage(ChatColor.GOLD + args[0] + "'s deaths:");
        for (var record : records) {
            var death = String.valueOf(record.deathId);
            String death_x = String.valueOf(record.x);
            String death_y = String.valueOf(record.y);
            String death_z = String.valueOf(record.z);
            UUID world = record.deathWorldId;
            String death_type = record.deathMessage;
            String item_count = String.valueOf(record.itemCount);
            LocalDateTime time = record.time;
            // Get difference of time
            String time_difference = getTimeDifference(time);
            String coordinates = String.format(" X=%s Y=%s Z=%s", death_x, death_y, death_z);
            // Send message with death info
            player.spigot().sendMessage(
                    new ComponentBuilder("#" + death + " ")
                            .color(ChatColor.WHITE)
                            .bold(true).append(time_difference + death_type + " at").color(ChatColor.GRAY)
                            .bold(false).append(coordinates).color(ChatColor.DARK_AQUA).bold(true)
                            .event(new HoverEvent(
                                    HoverEvent.Action.SHOW_TEXT, new Text(new ComponentBuilder("Click to teleport!").color(ChatColor.BLUE).create()))
                            )
                            .event(new ClickEvent(
                                    ClickEvent.Action.RUN_COMMAND, "/drteleport " + death_x + " " + death_y + " " + death_z + " " + world)
                            )
                            .append(" (" + item_count + " items)")
                            .color(ChatColor.DARK_AQUA).bold(true).event(new HoverEvent(
                                    HoverEvent.Action.SHOW_TEXT, new Text(new ComponentBuilder("Click to show inventory!").color(ChatColor.BLUE).create()))
                            )
                            .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/restoreinvsee " + args[0] + " " + death)).create()
            );
        }

        return true;
    }

    private static String getTimeDifference(LocalDateTime firstDate) {

        LocalDateTime secondDate = LocalDateTime.now(ZoneId.systemDefault());
        LocalDateTime cut = firstDate.plusDays(ChronoUnit.DAYS.between(firstDate, secondDate));
        Period period = Period.between(firstDate.toLocalDate(), cut.toLocalDate());
        Duration duration = Duration.between(cut, secondDate);
        String result = String.format("%sy %smt %sd %sh %sm %ss ago: ", period.getYears(), period.getMonths(),
                period.getDays(), duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart());
        // Removes time info if 0
        if (period.getYears() == 0) {
            result = result.replace("0y ", "");
        }
        if (period.getMonths() == 0) {
            result = result.replace("0mt ", "");
        }
        if (period.getDays() == 0) {
            result = result.replace("0d ", "");
        }
        if (duration.toHours() == 0) {
            result = result.replace("0h ", "");
        }
        if (duration.toMinutesPart() == 0) {
            result = result.replace("0m ", "");
        }
        return result;
    }

}
