/*
 * Copyright (C) 2019-2020 Chormi (hofill)
 *
 * This file is part of DeathRecovery.
 *
 * DeathRecovery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeathRecovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with DeathRecovery.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.cubekrowd.deathrecovery.events;

import net.cubekrowd.deathrecovery.DeathRecovery;
import net.cubekrowd.deathrecovery.records.DeathRecord;
import net.cubekrowd.deathrecovery.records.EncodedItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

public class PlayerDeath implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        int deathBlockX = player.getLocation().getBlockX();
        int deathBlockY = player.getLocation().getBlockY();
        int deathBlockZ = player.getLocation().getBlockZ();
        var worldId = player.getLocation().getWorld().getUID();
        var deathMessage = event.getDeathMessage();
        var items = player.getInventory().getContents();
        int itemCount = 0;
        int indexSize = 1;
        Map<Integer, EncodedItemStack> itemsMap = new HashMap<>();
        for (int i = 0; i < items.length; i ++) {
            ItemStack stack = items[i];
            if (stack != null) {
                itemCount++;
                itemsMap.put(i, EncodedItemStack.fromStack(stack));
                indexSize = Math.max(indexSize, i + 1);
            }
        }
        if (itemCount == 0) {
            // No need to log this
            return;
        }
        DeathRecovery.getInstance().getDeathDatabase().addRecord(
                new DeathRecord(player.getUniqueId(), DeathRecovery.getInstance().getDeathDatabase().getNextID(), deathBlockX, deathBlockY, deathBlockZ, worldId, deathMessage, LocalDateTime.now(ZoneId.systemDefault()), itemCount, indexSize, itemsMap)
        );
    }

}
