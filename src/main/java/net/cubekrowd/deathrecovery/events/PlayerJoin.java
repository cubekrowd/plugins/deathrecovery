/*
 * Copyright (C) 2019-2020 Chormi (hofill)
 *
 * This file is part of DeathRecovery.
 *
 * DeathRecovery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeathRecovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with DeathRecovery.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.cubekrowd.deathrecovery.events;

import net.cubekrowd.deathrecovery.records.DeathRecord;
import net.cubekrowd.deathrecovery.DeathRecovery;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerJoin implements Listener {
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        var recoverRecord = DeathRecovery.getInstance().getWaitingDatabase().getWaitingRecord(player.getUniqueId());
        if (recoverRecord == null) {
            return;
        }
        // See if the player's inventory has items in it
        if (getFull(player.getInventory())) {
            DeathRecord toGive = DeathRecovery.getInstance().getDeathDatabase().getRecord(recoverRecord.deathPlayerUUID, recoverRecord.deathId);
            if (toGive != null) {
                fillInv(toGive, player);
                player.sendMessage(ChatColor.GREEN + "Successfully recovered your inventory!");
                DeathRecovery.getInstance().getWaitingDatabase().removeFromWaitingList(player.getUniqueId());
            } else {
                player.sendMessage(ChatColor.RED + "Your death inventory expired or is invalid!");
                DeathRecovery.getInstance().getWaitingDatabase().removeFromWaitingList(player.getUniqueId());
            }
        } else {
            player.sendMessage(ChatColor.RED + "Could not restore your death inventory because your inventory is full!");
            player.sendMessage(ChatColor.RED + "Clear out your inventory and log back in!");
        }
    }

    private void fillInv(DeathRecord record, Player playerToAdd) {
        for (int itemIndex = 0; itemIndex < record.indexSize; itemIndex++) {
            ItemStack itemToAdd = record.getItemAt(itemIndex);
            playerToAdd.getInventory().setItem(itemIndex, itemToAdd);
        }
    }

    private static boolean getFull(Inventory inv) {
        for (ItemStack item : inv.getContents()) {
            if (item != null) {
                return false;
            }
        }
        return true;
    }

}
