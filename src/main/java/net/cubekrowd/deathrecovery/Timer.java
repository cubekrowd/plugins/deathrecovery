/*
 * Copyright (C) 2019-2020 Chormi (hofill)
 *
 * This file is part of DeathRecovery.
 *
 * DeathRecovery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeathRecovery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with DeathRecovery.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.cubekrowd.deathrecovery;

import lombok.Getter;
import net.cubekrowd.deathrecovery.records.DeathDatabase;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Timer {

    @Getter
    private BukkitTask task;

    @Getter
    private final Runnable runnable;

    @Getter
    private final long delay;

    @Getter
    private final Plugin plugin;

    public Timer(Plugin plugin, String delay, Runnable runnable) {
        long delayMilli = getMillis(delay);
        this.plugin = plugin;
        this.delay = delayMilli / 50;
        this.runnable = runnable;
    }

    public void start() {
        task = Bukkit.getScheduler().runTaskTimer(plugin, runnable, 0, delay);
    }

    public void stop() {
        task.cancel();
    }

    public static Timer pruneOld(Plugin plugin, Supplier<DeathDatabase> db, String delay, String oldest) {
        long timeToRemoveMilli = getMillis(oldest);
        return new Timer(plugin, delay, () -> {
            int counter = db.get().removeOldRecords(timeToRemoveMilli);
            if (counter > 0) {
                DeathRecovery.tellConsole("[DeathRecovery] Removed " + counter + " inventories!");
            }
        });
    }

    public static long getMillis(String stringToConvert) {
        int multipiler = 100000;
        if (stringToConvert.contains("w")) {
            multipiler = 604800000;
            stringToConvert = stringToConvert.replace("w", "");
        }
        if (stringToConvert.contains("d")) {
            multipiler = 86400000;
            stringToConvert = stringToConvert.replace("d", "");
        }
        if (stringToConvert.contains("h")) {
            multipiler = 3600000;
            stringToConvert = stringToConvert.replace("h", "");
        }
        if (stringToConvert.contains("m")) {
            multipiler = 60000;
            stringToConvert = stringToConvert.replace("m", "");
        }
        if (stringToConvert.contains("s")) {
            multipiler = 1000;
            stringToConvert = stringToConvert.replace("s", "");
        }
        return (long) parseInt(stringToConvert) * multipiler;
    }

    private static int parseInt(String string) {
        int x = -1;
        try {
            x = Integer.parseInt(string);
        } catch (Exception ex) {}
        return x;
    }

}
