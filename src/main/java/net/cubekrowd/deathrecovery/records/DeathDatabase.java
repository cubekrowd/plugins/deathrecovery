package net.cubekrowd.deathrecovery.records;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;
import net.cubekrowd.deathrecovery.DeathRecovery;
import net.cubekrowd.deathrecovery.storage.DeathFileHandler;
import org.bukkit.configuration.ConfigurationSection;

public class DeathDatabase {

    @Getter
    private final List<DeathRecord> deaths;
    private int nextId = 0;
    @Getter
    @Setter
    private boolean modified;

    public DeathDatabase(List<DeathRecord> deaths) {
        if (deaths == null) {
            deaths = new ArrayList<>();
        }
        this.deaths = deaths;
        for (DeathRecord record : deaths) {
            nextId = Math.max(nextId, record.deathId);
        }
        nextId += 1;
    }

    public int getNextID() {
        int next = nextId;
        nextId += 1;
        return next;
    }

    public static DeathDatabase convertOld(ConfigurationSection section) {
        ConfigurationSection players = section.getConfigurationSection("players");
        List<DeathRecord> records = new ArrayList<>();
        for (String key : players.getKeys(false)) {
            UUID ownerId = UUID.fromString(key);
            ConfigurationSection player = players.getConfigurationSection(key);
            for (String deathKey : player.getKeys(false)) {
                int deathId;
                try {
                     deathId = Integer.parseInt(deathKey);
                } catch (Exception e) {
                    continue;
                }
                records.add(loadRecord(ownerId, deathId, player.getConfigurationSection(deathKey)));
            }
        }
        DeathDatabase db = new DeathDatabase(records);
        db.modified = true;
        return db;
    }

    public static DeathRecord loadRecord(UUID ownerId, int deathId, ConfigurationSection deathSection) {
        if (deathSection == null) {
            return null;
        }

        int x = deathSection.getInt("death_x");
        int y = deathSection.getInt("death_y");
        int z = deathSection.getInt("death_z");
        UUID deathWorldId = UUID.fromString(deathSection.getString("world"));
        String deathMessage = deathSection.getString("death_type");
        int itemCount = deathSection.getInt("item_count");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime time = LocalDateTime.parse(deathSection.getString("server_time"), formatter);
        Map<Integer, EncodedItemStack> internalItemByIndex = new HashMap<>();
        var items = deathSection.getConfigurationSection("items");
        int indexSize = 0;
        if (items != null) {
            for (var itemIndex : items.getKeys(false)) {
                var itemNum = Integer.parseInt(itemIndex);
                var item = items.getItemStack(itemIndex);
                internalItemByIndex.put(itemNum, EncodedItemStack.fromStack(item));
                indexSize = Math.max(indexSize, itemNum + 1);
            }
        }
        return new DeathRecord(ownerId, deathId, x, y, z, deathWorldId, deathMessage, time, itemCount, indexSize, internalItemByIndex);
    }

    public List<DeathRecord> getRecords(UUID playerId) {
        return deaths.stream().filter(record -> record.ownerUUID.equals(playerId)).collect(Collectors.toList());
    }

    public DeathRecord getRecord(UUID playerId, int deathIdForPlayer) {
        List<DeathRecord> fromPlayer = getRecords(playerId);
        for (DeathRecord death : fromPlayer) {
            if (death.deathId == deathIdForPlayer) {
                return death;
            }
        }
        return null;
    }

    public DeathRecord getRecord(int deathId) {
        return deaths.stream().filter(record -> record.deathId == deathId).findFirst().orElse(null);
    }

    public int removeOldRecords(long minAgeMillis) {
        int start = deaths.size();
        LocalDateTime serverDate = LocalDateTime.now(ZoneId.systemDefault());
        deaths.removeIf(record -> minAgeMillis <= Math.abs(ChronoUnit.MILLIS.between(serverDate, record.time)));
        return start - deaths.size();
    }

    public void addRecord(DeathRecord record) {
        modified = true;
        deaths.add(record);
    }

    public void saveToDefaultLocation() {
        if (modified) {
            DeathFileHandler.getInstance().saveDeathDatabase(this, getDefaultFile());
            modified = false;
        }
    }

    public static File getDefaultFile() {
        return new File(DeathRecovery.getInstance().getDataFolder(), "deaths.json");
    }
}
