package net.cubekrowd.deathrecovery.records;

import net.cubekrowd.deathrecovery.DeathRecovery;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.logging.Level;

/**
 * A way to encode and decode an {@link ItemStack}. This will only decode when the data itself is requested.
 */
public class EncodedItemStack {

    private ItemStack value;
    private String data;

    private EncodedItemStack(ItemStack value, String data) {
        this.value = value;
        this.data = data;
    }

    public ItemStack getStack() {
        if (value == null) {
            decode();
        }
        return value;
    }

    public String getData() {
        if (data == null) {
            encode();
        }
        return data;
    }

    public void encode() {
        try {
            ByteArrayOutputStream io = new ByteArrayOutputStream();
            BukkitObjectOutputStream obj = new BukkitObjectOutputStream(io);
            obj.writeObject(value);
            obj.flush();
            data = new String(Base64.getEncoder().encode(io.toByteArray()));
        } catch (IOException e) {
            DeathRecovery.LOGGER.log(Level.SEVERE, "Couldn't serialize item! " + value, e);
        }
    }

    public void decode() {
        try {
            byte[] bytes = Base64.getDecoder().decode(data);
            ByteArrayInputStream in = new ByteArrayInputStream(bytes);
            BukkitObjectInputStream obj = new BukkitObjectInputStream(in);
            ItemStack stack = (ItemStack) obj.readObject();
            in.close();
            value = stack;
        } catch (IOException | ClassNotFoundException e) {
            DeathRecovery.LOGGER.log(Level.SEVERE, "Couldn't deserialize item!", e);
        }
    }

    public static EncodedItemStack fromStack(ItemStack stack) {
        return new EncodedItemStack(stack.clone(), null);
    }

    public static EncodedItemStack fromEncoded(String data) {
        return new EncodedItemStack(null, data);
    }

}
