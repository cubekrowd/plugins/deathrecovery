package net.cubekrowd.deathrecovery.records;

import lombok.Getter;
import lombok.Setter;
import net.cubekrowd.deathrecovery.DeathRecovery;
import net.cubekrowd.deathrecovery.storage.DeathFileHandler;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WaitingDatabase {

    @Getter
    private final List<WaitingRecord> records;

    @Getter
    @Setter
    private boolean modified;

    public WaitingDatabase(List<WaitingRecord> records) {
        if (records == null) {
            records = new ArrayList<>();
        }
        this.records = records;
    }

    public boolean addToWaitingList(UUID playerId, UUID deathPlayerId, int deathId) {
        if (records.stream().anyMatch(record -> record.givePlayerUUID.equals(playerId))) {
            // Can't have two inventories for the same person
            return false;
        }
        records.add(new WaitingRecord(playerId, deathPlayerId, deathId));
        modified = true;
        return true;
    }

    public boolean removeFromWaitingList(UUID givePlayerId) {
        if (records.removeIf(record -> record.givePlayerUUID.equals(givePlayerId))) {
            modified = true;
            return true;
        }
        return false;
    }

    public WaitingRecord getWaitingRecord(UUID playerId) {
        return records.stream().filter(record -> record.givePlayerUUID.equals(playerId)).findFirst().orElse(null);
    }

    public DeathRecord getFromWaitingList(DeathDatabase database, UUID playerId) {
        WaitingRecord record = getWaitingRecord(playerId);
        if (record == null) {
            return null;
        }
        return database.getRecord(record.deathPlayerUUID, record.deathId);
    }

    public void saveToDefaultLocation() {
        if (modified) {
            DeathFileHandler.getInstance().saveWaitingDatabase(this, getDefaultFile());
            modified = false;
        }
    }

    public static File getDefaultFile() {
        return new File(DeathRecovery.getInstance().getDataFolder(), "waiting.json");
    }

}
