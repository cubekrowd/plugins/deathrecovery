package net.cubekrowd.deathrecovery.records;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.UUID;

@Value
@AllArgsConstructor
public class WaitingRecord {

    /**
     * {@link UUID} of the player to give the inventory to
     */
    public UUID givePlayerUUID;

    /**
     * {@link UUID} of the player that died
     */
    public UUID deathPlayerUUID;

    /**
     * The ID of the death
     */
    public int deathId;

}
