package net.cubekrowd.deathrecovery.records;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.bukkit.inventory.ItemStack;

@Value
@AllArgsConstructor
public class DeathRecord {
    public UUID ownerUUID;
    public int deathId;
    public int x;
    public int y;
    public int z;
    public UUID deathWorldId;
    public String deathMessage;
    public LocalDateTime time;
    public int itemCount;
    public int indexSize;
    public Map<Integer, EncodedItemStack> items;

    public ItemStack getItemAt(int index) {
        EncodedItemStack stack = items.get(index);
        if (stack == null) {
            return null;
        }
        return stack.getStack();
    }

}
